import javax.ejb.Remote;
import javax.ejb.Stateless;

@Remote(MangerInterface.class)
@Stateless
public class Manger implements MangerInterface{
    @Override
    public Repas mangerDehors() {
        return  new Repas("Maffé",20.0,Boolean.TRUE);
    }
}
